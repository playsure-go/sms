package mitake

import (
	"bitbucket.org/playsure-go/convert"
	"bitbucket.org/playsure-go/rest"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
	"strings"
)

type MitakeQueryResponse struct {
	MsgId      string
	StatusCode string
	Error      string
	StatusTime string
	SmsPoint   int8
}

func (mqr *MitakeQueryResponse) Parse(body []byte) error {
	bodyStr := string(body)
	ss := strings.Split(bodyStr, "\r\n")
	for _, s := range ss {
		if s == "" {
			continue
		} else if strings.Contains(s, "\t") {
			pp := strings.Split(s, "\t")
			if len(pp) != 3 {
				return errors.New(fmt.Sprintf("unknown data: %s", s))
			}
			mqr.MsgId = pp[0]
			mqr.StatusCode = pp[1]
			mqr.StatusTime = pp[2]
			point, err := convert.StringToInt8(pp[3])
			if err != nil {
				return err
			}
			mqr.SmsPoint = point
		} else if strings.Contains(s, "=") {
			pp := strings.Split(s, "=")
			switch pp[0] {
			case "statuscode":
				mqr.StatusCode = pp[1]
			case "Error":
				mqr.Error = pp[1]
			default:
				return errors.New(fmt.Sprintf("unknown key name: %s", pp[0]))
			}
		} else {
			return errors.New(fmt.Sprintf("unknown data: %s", s))
		}
	}
	return nil
}

func QuerySMS(username string, password string, messageId string) (*MitakeQueryResponse, error) {
	url, err := netUrl.Parse("https://smsapi.mitake.com.tw/api/mtk/SmQuery")
	if err != nil {
		return nil, err
	}

	parameters := map[string]string{}
	parameters["username"] = username
	parameters["password"] = password
	parameters["msgid"] = messageId
	parameters["smsPointFlag"] = "1"

	headers := map[string]string{}
	headers["Content-Type"] = "application/x-www-form-urlencoded"

	client := rest.Client{}
	response, err2 := client.PostForm(url.String(), headers, parameters)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	mitakeQueryResponse := new(MitakeQueryResponse)
	err4 := mitakeQueryResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return mitakeQueryResponse, nil
}
