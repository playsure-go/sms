package mitake

import (
	"bitbucket.org/playsure-go/convert"
	"bitbucket.org/playsure-go/rest"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
	"regexp"
	"strings"
)

type MitakeResponse struct {
	ClientId     string
	MsgId        string
	StatusCode   string
	Error        string
	AccountPoint int32
	Duplicate    string
	SmsPoint     int8
}

func (mr *MitakeResponse) Parse(body []byte) error {
	bodyStr := string(body)
	ss := strings.Split(bodyStr, "\r\n")
	pattern := regexp.MustCompile("^\\[\\w+\\]$")
	for _, s := range ss {
		if s == "" {
			continue
		} else if pattern.MatchString(s) {
			cid := strings.Trim(s, "[]")
			mr.ClientId = cid
		} else if strings.Contains(s, "=") {
			pp := strings.Split(s, "=")
			switch pp[0] {
			case "msgid":
				mr.MsgId = pp[1]
			case "statuscode":
				mr.StatusCode = pp[1]
			case "Error":
				mr.Error = pp[1]
			case "AccountPoint":
				point, err := convert.StringToInt32(pp[1])
				if err != nil {
					return err
				}
				mr.AccountPoint = point
			case "Duplicate":
				mr.Duplicate = pp[1]
			case "smsPoint":
				point, err := convert.StringToInt8(pp[1])
				if err != nil {
					return err
				}
				mr.SmsPoint = point
			default:
				return errors.New(fmt.Sprintf("unknown key name: %s", pp[0]))
			}
		} else {
			return errors.New(fmt.Sprintf("unknown data: %s", s))
		}
	}
	return nil
}

func SendSMS(username string, password string, mobile string, message string) (*MitakeResponse, error) {
	url, err := netUrl.Parse("https://smsapi.mitake.com.tw/api/mtk/SmSend?CharsetURL=UTF-8")
	if err != nil {
		return nil, err
	}

	parameters := map[string]string{}
	parameters["username"] = username
	parameters["password"] = password
	parameters["dstaddr"] = mobile
	// parameters["destname"] = destname
	// parameters["dlvtime"] = "60"
	// parameters["vldtime"] = "3600"
	parameters["smbody"] = message
	// parameters["response"] = response
	// parameters["clientid"] = clientId
	// parameters["objectID"] = objectID
	parameters["smsPointFlag"] = "1"

	headers := map[string]string{}
	headers["Content-Type"] = "application/x-www-form-urlencoded"

	client := rest.Client{}
	response, err2 := client.PostForm(url.String(), headers, parameters)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	mitakeResponse := new(MitakeResponse)
	err4 := mitakeResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return mitakeResponse, nil
}
