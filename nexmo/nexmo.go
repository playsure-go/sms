package nexmo

import (
	"fmt"
	"github.com/nexmo-community/nexmo-go"
	"net/http"
	"strings"
)

type NexmoResponse struct {
	SendSMSResponse *nexmo.SendSMSResponse
	HttpResponse    *http.Response
}

func SendSms(key string, secret string, from string, to string, message string) (*NexmoResponse, error) {
	auth := nexmo.NewAuthSet()
	auth.SetAPISecret(key, secret)

	if strings.HasPrefix(to, "09") {
		to = fmt.Sprintf("886%s", to[1:])
	}

	client := nexmo.NewClient(http.DefaultClient, auth)
	request := nexmo.SendSMSRequest{
		Type: nexmo.MessageTypeUnicode,
		From: from,
		To:   to,
		Text: message,
	}

	sendSMSResponse, httpResponse, err := client.SMS.SendSMS(request)
	if err != nil {
		return nil, err
	}

	nexmoResponse := new(NexmoResponse)
	nexmoResponse.SendSMSResponse = sendSMSResponse
	nexmoResponse.HttpResponse = httpResponse

	return nexmoResponse, nil
}
