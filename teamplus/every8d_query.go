package teamplus

import (
	"bitbucket.org/playsure-go/rest"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
)

type Every8dQueryResponse struct {
	SmsCount uint                        `json:"SMS_COUNT"`
	BatchId  string                      `json:"BID"`
	Data     []Every8dQueryResponse_Data `json:"DATA"`
}

type Every8dQueryResponse_Data struct {
	MR           string `json:"MR"`
	Name         string `json:"NAME"`
	Mobile       string `json:"MOBILE"`
	SendTime     string `json:"SEND_TIME"`
	Cost         string `json:"COST"`
	Status       string `json:"STATUS"`
	RealCost     string `json:"REAL_COST"`
	ReceivedTime string `json:"RECEIVED_TIME"`
}

func (eqr *Every8dQueryResponse) Parse(body []byte) error {
	var r Every8dQueryResponse
	err := json.Unmarshal(body, &r)
	if err != nil {
		return err
	}
	*eqr = r
	return nil
}

func QuerySMS(userId string, password string, batchId string) (*Every8dQueryResponse, error) {
	url, err := netUrl.Parse("https://api.e8d.tw/API21/HTTP/sendSMS.ashx")
	if err != nil {
		return nil, err
	}

	parameters := make(map[string]string)
	parameters["UID"] = userId
	parameters["PWD"] = password
	parameters["BID"] = batchId
	parameters["PNO"] = "1"
	parameters["RESPFORMAT"] = "1" // JSON

	headers := make(map[string]string)
	headers["Content-Type"] = "application/x-www-form-urlencoded"

	client := rest.Client{}
	response, err2 := client.PostForm(url.String(), headers, parameters)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	every8dQueryResponse := new(Every8dQueryResponse)
	err4 := every8dQueryResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return every8dQueryResponse, nil
}
