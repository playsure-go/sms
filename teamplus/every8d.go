package teamplus

import (
	"bitbucket.org/playsure-go/rest"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
	"strings"
)

type Every8dResponse struct {
	Credit  string
	Sended  string
	Cost    string
	Unsend  string
	BatchId string
	Status  string
	Message string
}

func (er *Every8dResponse) Parse(body []byte) error {
	bodyLines := strings.Split(string(body), "\r\n\r\n")
	index := len(bodyLines) - 1
	bodyLine := bodyLines[index]
	if strings.HasPrefix(bodyLine, "-") { // Starting with "-" means the error is occurred.
		result := strings.Split(bodyLine, ",")
		if len(result) != 2 {
			return errors.New(fmt.Sprintf("unparsed response: %s", bodyLine))
		}
		er.Status = result[0]
		er.Message = result[1]
	} else { // Success.
		result := strings.Split(bodyLine, ",")
		if len(result) != 5 {
			return errors.New(fmt.Sprintf("unparsed response: %s", bodyLine))
		}
		er.Credit = result[0]
		er.Sended = result[1]
		er.Cost = result[2]
		er.Unsend = result[3]
		er.BatchId = result[4]
	}
	return nil
}

func SendSMS(userId string, password string, subject string, mobile string, message string) (*Every8dResponse, error) {
	url, err := netUrl.Parse("https://api.e8d.tw/API21/HTTP/sendSMS.ashx")
	if err != nil {
		return nil, err
	}

	if strings.HasPrefix(mobile, "09") {
		mobile = fmt.Sprintf("+886%s", mobile[1:])
	}

	parameters := make(map[string]string)
	parameters["UID"] = userId
	parameters["PWD"] = password
	parameters["SB"] = subject
	parameters["MSG"] = message
	parameters["DEST"] = mobile
	parameters["ST"] = ""
	// parameters["RETRYTIME"] = "1440"

	headers := make(map[string]string)
	headers["Content-Type"] = "application/x-www-form-urlencoded"

	client := rest.Client{}
	response, err2 := client.PostForm(url.String(), headers, parameters)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	every8dResponse := new(Every8dResponse)
	err4 := every8dResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return every8dResponse, nil
}
