module bitbucket.org/playsure-go/sms

go 1.19

require (
	bitbucket.org/playsure-go/convert v0.0.4
	bitbucket.org/playsure-go/rest v0.0.3
	github.com/nexmo-community/nexmo-go v0.8.1
)

require (
	github.com/dghubble/sling v1.3.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/google/go-querystring v1.0.0 // indirect
)
