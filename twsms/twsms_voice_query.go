package twsms

import (
	"bitbucket.org/playsure-go/convert"
	"bitbucket.org/playsure-go/rest"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
	"strings"
)

type TwsmsVoiceQueryResponse struct {
	MsgId      uint64
	Mobile     string
	Playtime   uint
	TotalPoint uint
	Status     string
	ErrorCode  int
}

func (tvr *TwsmsVoiceQueryResponse) Parse(data []byte) error {
	dataLines := strings.Split(string(data), "\r\n\r\n")
	index := len(dataLines) - 1
	dataLine := dataLines[index]
	if !strings.HasPrefix(dataLine, "resp=") {
		return errors.New(fmt.Sprintf("unknown data: %s", dataLine))
	}
	dataLine = dataLine[len("resp="):]

	if strings.HasPrefix(dataLine, "-") { // Starting with "-" means the error is occurred.
		result := strings.Split(dataLine, ",")
		if len(result) != 1 {
			return errors.New(fmt.Sprintf("unparsed value: %s", dataLine))
		}

		errorCode, err := convert.StringToInt(result[0])
		if err != nil {
			return err
		}
		tvr.ErrorCode = errorCode
	} else { // Success.
		result := strings.Split(dataLine, ",")
		if len(result) != 5 {
			return errors.New(fmt.Sprintf("unparsed value: %s", dataLine))
		}

		msgId, err := convert.StringToUint64(result[0])
		if err != nil {
			return err
		}
		tvr.MsgId = msgId

		tvr.Mobile = result[1]

		playtime, err2 := convert.StringToUint(result[2])
		if err2 != nil {
			return err2
		}
		tvr.Playtime = playtime

		totalPoint, err3 := convert.StringToUint(result[3])
		if err3 != nil {
			return err3
		}
		tvr.TotalPoint = totalPoint

		tvr.Status = result[4]
	}
	return nil
}

func QueryVoice(username string, password string, messageId uint64) (*TwsmsVoiceQueryResponse, error) {
	url, err := netUrl.Parse("https://api.twsms.com/query_voice.php")
	if err != nil {
		return nil, err
	}

	msgid := convert.Uint64ToString(messageId)

	query := url.Query()
	query.Set("username", username)
	query.Set("password", password)
	query.Set("msgid", msgid)
	url.RawQuery = query.Encode()

	client := rest.Client{}
	response, err2 := client.Get(url.String(), nil)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	twsmsVoiceQueryResponse := new(TwsmsVoiceQueryResponse)
	err4 := twsmsVoiceQueryResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return twsmsVoiceQueryResponse, nil
}
