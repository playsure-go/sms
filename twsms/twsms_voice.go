package twsms

import (
	"bitbucket.org/playsure-go/convert"
	"bitbucket.org/playsure-go/rest"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
	"strings"
)

type TwsmsVoiceResponse struct {
	MsgId      uint64
	TotalPoint uint
	ErrorCode  int
}

func (tvr *TwsmsVoiceResponse) Parse(data []byte) error {
	dataLines := strings.Split(string(data), "\r\n\r\n")
	index := len(dataLines) - 1
	dataLine := dataLines[index]
	if !strings.HasPrefix(dataLine, "resp=") {
		return errors.New(fmt.Sprintf("unknown data: %s", dataLine))
	}
	dataLine = dataLine[len("resp="):]

	if strings.HasPrefix(dataLine, "-") { // Starting with "-" means the error is occurred.
		result := strings.Split(dataLine, ",")
		if len(result) != 1 {
			return errors.New(fmt.Sprintf("unparsed value: %s", dataLine))
		}

		errorCode, err := convert.StringToInt(result[0])
		if err != nil {
			return err
		}
		tvr.ErrorCode = errorCode
	} else { // Success.
		result := strings.Split(dataLine, ",")
		if len(result) != 2 {
			return errors.New(fmt.Sprintf("unparsed value: %s", dataLine))
		}

		msgId, err := convert.StringToUint64(result[0])
		if err != nil {
			return err
		}
		tvr.MsgId = msgId

		totalPoint, err2 := convert.StringToUint(result[1])
		if err2 != nil {
			return err2
		}
		tvr.TotalPoint = totalPoint
	}
	return nil
}

func SendVoice(username string, password string, mobile string, message string) (*TwsmsVoiceResponse, error) {
	url, err := netUrl.Parse("https://api.twsms.com/send_voice.php")
	if err != nil {
		return nil, err
	}

	query := url.Query()
	query.Set("username", username)
	query.Set("password", password)
	query.Set("type", "1")
	query.Set("phone", mobile)
	query.Set("message", message)
	// query.Set("voice", "1")
	// query.Set("speed", "0")
	// query.Set("vol", "100")
	// query.Set("voice_url", voiceUrl)
	// query.Set("response", responseUrl)
	url.RawQuery = query.Encode()

	client := rest.Client{}
	response, err2 := client.Get(url.String(), nil)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	twsmsVoiceResponse := new(TwsmsVoiceResponse)
	err4 := twsmsVoiceResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return twsmsVoiceResponse, nil
}
