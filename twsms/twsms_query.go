package twsms

import (
	"bitbucket.org/playsure-go/convert"
	"bitbucket.org/playsure-go/rest"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
)

type TwsmsQueryResponse struct {
	Code       string `json:"code"`
	Text       string `json:"text"`
	StatusCode string `json:"statuscode"`
	StatusText string `json:"statustext"`
	DoneTime   string `json:"donetime"`
}

func (tqr *TwsmsQueryResponse) Parse(data []byte) error {
	var r TwsmsQueryResponse
	err := json.Unmarshal(data, &r)
	if err != nil {
		return err
	}
	*tqr = r
	return nil
}

func QuerySMS(username string, password string, mobile string, messageId uint64) (*TwsmsQueryResponse, error) {
	url, err := netUrl.Parse("https://api.twsms.com/json/sms_query.php")
	if err != nil {
		return nil, err
	}

	msgid := convert.Uint64ToString(messageId)

	query := url.Query()
	query.Set("username", username)
	query.Set("password", password)
	// query.Set("deltime", "Y")
	// query.Set("checkpoint", "Y")
	query.Set("mobile", mobile)
	query.Set("msgid", msgid)
	// query.Set("outrange", "Y")
	url.RawQuery = query.Encode()

	client := rest.Client{}
	response, err2 := client.Get(url.String(), nil)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	twsmsQueryResponse := new(TwsmsQueryResponse)
	err4 := twsmsQueryResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return twsmsQueryResponse, nil
}
