package twsms

import (
	"bitbucket.org/playsure-go/rest"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	netUrl "net/url"
)

type TwsmsResponse struct {
	Code  string `json:"code"`
	Text  string `json:"text"`
	MsgId uint64 `json:"msgid"`
}

func (tr *TwsmsResponse) Parse(data []byte) error {
	var r TwsmsResponse
	err := json.Unmarshal(data, &r)
	if err != nil {
		return err
	}
	*tr = r
	return nil
}

func SendSMS(username string, password string, mobile string, message string) (*TwsmsResponse, error) {
	url, err := netUrl.Parse("https://api.twsms.com/json/sms_send.php")
	if err != nil {
		return nil, err
	}

	query := url.Query()
	query.Set("username", username)
	query.Set("password", password)
	// query.Set("sendtime", "201203121830")
	// query.Set("expirytime", "28800")
	// query.Set("mo", "N")
	query.Set("mobile", mobile)
	query.Set("message", message)
	// query.Set("drurl", drurl)
	// query.Set("drurl_check", "Y")
	// query.Set("lmsg", "Y")
	url.RawQuery = query.Encode()

	client := rest.Client{}
	response, err2 := client.Get(url.String(), nil)
	if err2 != nil {
		return nil, err2
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("%d %s", response.StatusCode, response.Status)
		return nil, errors.New(errStr)
	}

	body, err3 := io.ReadAll(response.Body)
	if err3 != nil {
		return nil, err3
	}

	twsmsResponse := new(TwsmsResponse)
	err4 := twsmsResponse.Parse(body)
	if err4 != nil {
		return nil, err4
	}

	return twsmsResponse, nil
}
